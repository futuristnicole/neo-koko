import React from 'react';


const Header = () => {
    return (
        <div className="brand-thrird-l top-logo">
            <h1 className="hero-text ">
                <span className="logo">KOKO</span>
                <span className="hero-text__sub">How Far You Can Go To Pay Tribute To Your Love?</span>
            </h1>
        </div>
    );
};

export default Header;