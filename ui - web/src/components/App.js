import React, { Component } from 'react';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
// import Header from './Header';
import MainNav from './sections/MainNav';
import Header from './sections/Header';
import Home from './pages/Home';
import Trailers from './pages/Trailers';
import PhotoGallery from './pages/PhotoGallery';
import Credits from './pages/Credits';
import About from './pages/About';
import Contact from './pages/Contact';


class App extends Component {
  render() {
    return (
      <div className="">
        {/* <Header /> */}
        <BrowserRouter>
          <MainNav />
          <Switch>
            <Route path="/" exact component={Home} />
            <Route path="/"  component={Header} />
          </Switch>
          <Route path="/trailers" exact component={Trailers} />
          <Route path="/photogallery" exact component={PhotoGallery} />
          <Route path="/credits" exact component={Credits} />
          <Route path="/about" exact component={About} />
          <Route path="/contact" exact component={Contact} />
        </BrowserRouter>
      </div>
    );
  }
}

export default App;
