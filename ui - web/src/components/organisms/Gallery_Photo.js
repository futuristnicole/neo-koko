import React, { Component } from 'react';
import { Link } from 'react-router-dom';

class Gallery_Photo extends Component {
    render() {
        return (
            // <div className={`gallery__item--${this.props.size}`}>
                <Link to={`/photogallery/${this.props.file}`}> 
                    <img className="gallery__img" 
                        src={`/img/${this.props.location}/${this.props.file}.jpg`} 
                        alt={`${this.props.alt}`} >
                    </img>
                </Link>
            // </div>
        )
    }
};

export default Gallery_Photo;