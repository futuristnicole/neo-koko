import React, { Component } from 'react';
import { Link } from 'react-router-dom';


class PersonBox extends Component {
    render() {
        return (
            <div className="color-box">
              <img className="img-contain__staring-home" src={`/img/headshots/${this.props.name}.jpg`} alt={this.props.name} />
                <div className> <br />
                    <p className="actor">{this.props.name}</p>
                    <p className="playing">as {this.props.roll}</p>
                    <Link to={`/credits/${this.props.name}`} >more info</Link>
                </div>
            </div>
        );
    }
};

export default PersonBox;