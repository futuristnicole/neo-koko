import React from 'react';
import AllPictures from '../sections/photoGallery/AllPictures';

const PhotoGallery = () => {
    return (
        <section className="center">
            {/* <p>PhotoGallery</p> */}
            <AllPictures />
        </section>
    );
};

export default PhotoGallery;