import React, { Component } from 'react';
import HomeHero from '../sections/home/HomeHero';
import HomeAbout from '../sections/home/HomeAbout';
import HomeKoko from '../sections/home/HomeKoko';
import HomeStar from '../sections/home/HomeStar';


class Home extends Component {
    render() {
        return (
            <div>
                <HomeHero />
                <HomeAbout />
                <HomeKoko />
                <HomeStar />
            </div>
        );
    }
};

export default Home;