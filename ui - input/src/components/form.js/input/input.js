import React from 'react';

const input = (props) => {
    let inputElement = null;
    switch (props.inputtype) {
        case ('input'):
            inputElement = <input {...props} />;
        case ('textarea'):
                inputElement = <textarea {...props}/>;
        default:
            inputElement = <input {...props}/>;
    }
    return (
        <div> 
            <label>{props.label}</label>
            {inputElement}
        </div>
    );
};

export default input