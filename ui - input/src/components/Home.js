import React, { Component } from "react";
import { Link } from 'react-router-dom';


class Home extends Component {
 

  render() {    
    return (
      <div>
        <Link to="/personlist">Persons</Link>
          <br></br>
        <Link to="/photolist">Photos</Link>
      </div>
    )
  }
}


export default Home;