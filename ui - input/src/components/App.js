import React, { Component } from "react";
import "./App.css";
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import Home from "./Home";
import PersonList from "./person/PersonList";
import PersonCreate from "./person/PersonCreate";
import PersonDetail from "./person/PersonDetail";
import PhotoList from "./photo/PhotoList";
import PhotoCreate from "./photo/PhotoCreate";
import PhotoDetail from "./photo/PhotoDetail";

class App extends Component {
  render() {
    return (
      <div className="App">
        <header className="App-header">
          <img
            src={process.env.PUBLIC_URL + "/img/grandstack.png"}
            className="App-logo"
            alt="logo"
          />
          <h1 className="App-title">Welcome to GRANDstack</h1>
        </header>
        <Router>
          <Switch>
            <Route path="/" exact component={Home}/>
            <Route path="/personlist" exact component={PersonList}/>
            <Route path="/personlist/new" exact component={PersonCreate}/>
            <Route path="/personlist/:id" exact component={PersonDetail}/>
            <Route path="/photolist" exact component={PhotoList}/>
            <Route path="/photolist/new" exact component={PhotoCreate}/>
            <Route path="/photolist/:id" exact component={PhotoDetail}/>
          </Switch>
        </Router>
      </div>
    );
  }
} 

export default App;
