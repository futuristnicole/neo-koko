import React, { Component } from "react";
// import { Link } from 'react-router-dom';
import { graphql } from "react-apollo";
import AddPersonRoll from '../../queries/person/AddPersonRoll';
// import CreateRoll from '../../queries/person/CreateRoll';

class RollCreate extends Component {
    constructor(props) {
        super(props);
        this.state = { Roll: '' };
    }
    onSubmit(event) { 
        event.preventDefault();
        this.props.mutate({
            variables: {
                PersonInput: this.state.CreateRoll,
                RollInput: this.state.RollInput
            }
        })
    }
    render() {
        return (
            <div>
                CreateRoll
                <form onSubmit={this.onSubmit.bind(this)}>
                    <label>Add a Roll</label>
                    <input 
                        value={this.state.content}
                        onChange={event => this.setState({ content: event.target.value})} /> 
                </form>
            </div>
        )
    }
}

export default graphql(AddPersonRoll)(RollCreate);