import React, { Component } from "react";
import { graphql } from "react-apollo";
import { Link } from 'react-router-dom';
import FindPerson from '../../queries/person/FindPerson';
import RollCreate from './RollCreate';


class PersonDetail extends Component {
    renderPersons() {
        return this.props.data.Person.map(({ id, name }) => {
        return (
            <div key={id}>
            <h3>{name}</h3>  
            <RollCreate />
            </div>
        );
        })
    }
    render() {
        console.log(this.props);
        if (this.props.data.loading) { return <div>Loading...</div>; } 
        
        return (
        <div>
            <Link to="/personlist">Person List</Link>
            {this.renderPersons()}
            
        </div>
        )
    }
}


export default graphql(FindPerson, {  options: (props) => { return { variables: { id: props.match.params.id }}}})(PersonDetail);