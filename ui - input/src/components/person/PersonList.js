import React, { Component } from "react";
import { graphql } from "react-apollo";
// import gql from "graphql-tag";
import { Link } from 'react-router-dom';
import FetchPersons from '../../queries/person/FetchPersons';
import DeletePerson from '../../queries/person/DeletePerson';
import PersonCreate from './PersonCreate';
  
class PersonList extends Component {
    onSongDelete(id) {
        this.props.mutate({ variables: { id }})
            .then(() => this.props.data.refetch());
    }
    renderPersons() {
        return this.props.data.Person.map(({ id, name }) => {
        return (
            <div key={id}>
            <Link to={`/personlist/${id}`}>{name}</Link>  <span> - - </span>
            <i onClick={() => this.onSongDelete(id)}>X</i>
            </div>
        );
        })
    }

    render() {
        console.log(this.props);
        if (this.props.data.loading) { return <div>Loading...</div>; } 
        
        return (
        <div>
            {this.renderPersons()}
            
            <PersonCreate />
            {/* <Link to="/personlist/new">Add Perosn</Link> */}
        </div>
        )
    }
}


export default graphql(DeletePerson)(
    graphql(FetchPersons)(PersonList)
);