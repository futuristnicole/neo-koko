import React, { Component } from "react";
import { graphql } from "react-apollo";
// import { Link } from 'react-router-dom';
import FetchPersons from '../../queries/person/FetchPersons';
import AddPerson from '../../queries/person/AddPerson';


class PersonCreate extends Component {
    constructor(props) {
        super(props);
        this.state = { name: ''};
    }
    onSubmit(event) {
        event.preventDefault();
        this.props.mutate({
            variables: {    name: this.state.name, 
                            description: this.state.description, 
                            // headShot: this.state.headShot 
                        },
            refetchQueries: [{ query: FetchPersons }]
        });
        // need to send user back to personlist after added name
    }
    render() {
        return (
            <div>
                {/* <Link to="/personlist">Back</Link> */}
                <h3>Add person</h3>
                <form onSubmit={this.onSubmit.bind(this)}>
                    <label>Person Name</label>
                    <input 
                        onChange={event => this.setState({ name: event.target.value })}
                        value={this.state.name}
                    /> <br></br>
                    <label>Person Description</label>
                    <input 
                        onChange={event => this.setState({ description: event.target.value })}
                        value={this.state.description}
                    />
                    <button>Submit</button>
                </form>
            </div>
        )
    }
}



export default graphql(AddPerson)(PersonCreate);