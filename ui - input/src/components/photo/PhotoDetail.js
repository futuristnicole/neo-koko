import React, { Component } from "react";
import { graphql } from "react-apollo";
import { Link } from 'react-router-dom';
import FindPhoto from '../../queries/photo/FindPhoto';



class PhotoDetail extends Component {
    renderPhotos() {
        return this.props.data.Photo.map(({ id, title, alt, location, size }) => {
        return (
            <div key={id}>
            <h3>{title}</h3>  
            <p>{alt}</p>
            <p>{location}</p>
            <p>{size}</p>
           
            {/* <RollCreate /> */}
            </div>
        );
        })
    }
    render() {
        console.log(this.props);
        if (this.props.data.loading) { return <div>Loading...</div>; } 
        
        return (
        <div>
            <Link to="/photolist">Photo List</Link>
            {this.renderPhotos()}
            
        </div>
        )
    }
}


export default graphql(FindPhoto, {  options: (props) => { return { variables: { id: props.match.params.id }}}})(PhotoDetail);