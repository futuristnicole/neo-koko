import React, { Component } from "react";
import { graphql } from "react-apollo";
// import gql from "graphql-tag";
import { Link } from 'react-router-dom';
import FetchPhotos from '../../queries/photo/FetchPhotos';
import DeletePhoto from '../../queries/photo/DeletePhoto';
import PhotoCreate from './PhotoCreate';
  
class PhotoList extends Component {
    onPhotoDelete(id) {
        this.props.mutate({ variables: { id }})
            .then(() => this.props.data.refetch());
    }
    renderPhotos() {
        return this.props.data.Photo.map(({ id, file }) => {
        return (
            <div key={id}>
            <Link to={`/photolist/${id}`}>{file}</Link>  <span> - - </span>
            <i onClick={() => this.onPhotoDelete(id)}>X</i>
            </div>
        );
        })
    }

    render() {
        console.log(this.props);
        if (this.props.data.loading) { return <div>Loading...</div>; } 
        
        return (
        <div>
            {this.renderPhotos()}
            
            <PhotoCreate />
            {/* <Link to="/photolist/new">Add Perosn</Link> */}
        </div>
        )
    }
}


export default graphql(DeletePhoto)(
    graphql(FetchPhotos)(PhotoList)
);