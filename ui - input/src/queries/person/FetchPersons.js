import gql from "graphql-tag";

const FetchPersons = gql`
  {
    Person{
      id
      name
    }
  }
`;

export default FetchPersons;