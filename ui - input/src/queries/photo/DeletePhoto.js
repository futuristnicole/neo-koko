import gql from "graphql-tag";

const DeletePhoto = gql`
mutation DeletePhoto($id: ID!) {
    DeletePhoto(id: $id) {
      id
    }
  }
`;
export default DeletePhoto;