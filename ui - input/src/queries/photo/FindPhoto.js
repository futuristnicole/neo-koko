import gql from "graphql-tag";

const FindPhoto = gql`
    query FindPhoto($id: ID!) {
        Photo(id: $id) {
            file
            id
            alt
            location
            size
          
        }
    }
`;

export default FindPhoto;