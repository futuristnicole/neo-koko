import gql from "graphql-tag";

const FetchPhotos = gql`
  query Photo {
    Photo {
      id
      file
      alt
      location
      size
    
    }
  }
`;

export default FetchPhotos;